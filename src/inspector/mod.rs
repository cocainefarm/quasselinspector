use egui::Stroke;
use libquassel::session::SessionManager;

use crate::Drawable;

#[derive(Debug, Default)]
pub struct InspectorState {
    view: InspectorView,
}

#[derive(Debug, Default, PartialEq, Eq, PartialOrd, Ord)]
pub enum InspectorView {
    #[default]
    AliasManager,
    BufferSyncer,
    BufferViewManager,
    CertManager,
    CoreInfo,
    HighlightRuleManager,
    Identities,
    IgnoreListManager,
    Networks,
}

impl crate::State {
    pub fn inspector(&mut self, ui: &mut egui::Ui) -> egui::Response {
        ui.centered_and_justified(|ui| {
            ui.add_space(10.0);
            ui.horizontal(|ui| {
                ui.add_space(10.0);
                ui.vertical(|ui| {
                    let style = ui.style_mut();
                    style.visuals.selection.bg_fill = style.visuals.extreme_bg_color;
                    style.visuals.selection.stroke = Stroke::new(10.0, style.visuals.warn_fg_color);

                    ui.selectable_value(
                        &mut self.inspector.view,
                        InspectorView::AliasManager,
                        "AliasManager",
                    );
                    ui.selectable_value(
                        &mut self.inspector.view,
                        InspectorView::BufferSyncer,
                        "BufferSyncer",
                    );
                    ui.selectable_value(
                        &mut self.inspector.view,
                        InspectorView::BufferViewManager,
                        "BufferViewManager",
                    );
                    ui.selectable_value(
                        &mut self.inspector.view,
                        InspectorView::CertManager,
                        "CertManager",
                    );
                    ui.selectable_value(
                        &mut self.inspector.view,
                        InspectorView::CoreInfo,
                        "CoreInfo",
                    );
                    ui.selectable_value(
                        &mut self.inspector.view,
                        InspectorView::HighlightRuleManager,
                        "HighlightRuleManager",
                    );
                    ui.selectable_value(
                        &mut self.inspector.view,
                        InspectorView::Identities,
                        "Identities",
                    );
                    ui.selectable_value(
                        &mut self.inspector.view,
                        InspectorView::IgnoreListManager,
                        "IgnoreListManager",
                    );
                    ui.selectable_value(
                        &mut self.inspector.view,
                        InspectorView::Networks,
                        "Networks",
                    );
                });
                ui.add_space(10.0);

                ui.separator();

                ui.add_space(10.0);
                ui.vertical(|ui| {
                    egui::ScrollArea::vertical()
                        .auto_shrink([false, false])
                        .drag_to_scroll(false)
                        .show(ui, |ui| {
                            let quassel = self.quassel.read();
                            match self.inspector.view {
                                InspectorView::AliasManager => {
                                    quassel.alias_manager.draw(ui, self.ui_channel_tx.clone())
                                }
                                InspectorView::BufferSyncer => {
                                    quassel.buffer_syncer.draw(ui, self.ui_channel_tx.clone())
                                }
                                InspectorView::BufferViewManager => quassel
                                    .buffer_view_manager
                                    .draw(ui, self.ui_channel_tx.clone()),
                                InspectorView::CertManager => {
                                    quassel.cert_manager.draw(ui, self.ui_channel_tx.clone())
                                }
                                InspectorView::CoreInfo => {
                                    quassel.core_info.draw(ui, self.ui_channel_tx.clone())
                                }
                                InspectorView::HighlightRuleManager => quassel
                                    .highlight_rule_manager
                                    .draw(ui, self.ui_channel_tx.clone()),
                                InspectorView::Identities => {
                                    quassel.identities.draw(ui, self.ui_channel_tx.clone())
                                }
                                InspectorView::IgnoreListManager => quassel
                                    .ignore_list_manager
                                    .draw(ui, self.ui_channel_tx.clone()),
                                InspectorView::Networks => {
                                    quassel.networks.iter().for_each(|(id, network)| {
                                        network.draw(ui, self.ui_channel_tx.clone())
                                    });
                                }
                            };
                        })
                });
            });
        })
        .response
    }

    // TODO do the match to use .show_viewport and only
    // render the elements that are currently in view
    // https://docs.rs/egui_extras/latest/egui_extras/struct.TableBody.html#method.heterogeneous_rows
    pub fn logs(&mut self, ui: &mut egui::Ui) {
        egui::ScrollArea::vertical()
            .stick_to_bottom(true)
            .show(ui, |ui| {
                for (direction, line) in self.logs.read().iter() {
                    ui.with_layout(egui::Layout::left_to_right(egui::Align::LEFT), |ui| {
                        ui.add_space(10.0);
                        ui.label(format!("{:#?}", direction));
                        ui.separator();
                        ui.add(egui::widgets::Label::new(format!("{:#?}", line)).wrap());
                    });
                    ui.add_space(10.0)
                }
            });
    }
}
