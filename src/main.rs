use std::{sync::Arc, thread};

use crossbeam_channel::{bounded, Sender};
use eframe::egui;
use egui::{mutex::RwLock, Color32};
use tracing::debug;

use crate::{
    inspector::InspectorState,
    server::{ClientState, Direction, Server},
};

mod inspector;
mod server;

mod objects;

use once_cell::sync::OnceCell;

static CTX: OnceCell<egui::Context> = OnceCell::new();

pub struct StateTracker {
    server: server::Server,
    state: State,

    dock_state: egui_dock::DockState<String>,
}

// #[derive(Default)]
pub struct State {
    inspector: inspector::InspectorState,
    quassel: server::QuasselState,
    logs: server::Log,

    ui_channel_tx: Sender<server::UiMessage>,
}

static BACKGROUND_COLOR: Color32 = Color32::from_rgb(24, 24, 24);

impl eframe::App for StateTracker {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        let frame = egui::Frame::none().fill(BACKGROUND_COLOR);

        egui::CentralPanel::default().frame(frame).show(ctx, |ui| {
            // ui.style_mut().debug.debug_on_hover = true;
            if self.server.connected {
                // FIXME not available like this anymore
                // let mut style = egui_dock::Style::from_egui(ui.style().as_ref());
                // style.show_close_buttons = false;
                // style.tab_background_color = BACKGROUND_COLOR;

                egui_dock::DockArea::new(&mut self.dock_state)
                    // .style(style)
                    .show_inside(ui, &mut self.state);
            } else {
                self.server.draw(ui, self.state.ui_channel_tx.clone());
            }
        });
    }
}

impl egui_dock::TabViewer for State {
    type Tab = String;

    fn ui(&mut self, ui: &mut egui::Ui, tab: &mut Self::Tab) {
        match tab.as_str() {
            "Inspector" => {
                self.inspector(ui);
            }
            "Logs" => {
                self.logs(ui);
            }
            _ => unreachable!(),
        }
    }

    fn title(&mut self, tab: &mut Self::Tab) -> egui::WidgetText {
        (&*tab).into()
    }
}

pub fn main() {
    /*
     * setup tracing
     */
    use tracing::{debug, metadata::LevelFilter, Level};
    use tracing_subscriber::{fmt::format::FmtSpan, prelude::*, EnvFilter};

    let filter = tracing_subscriber::filter::Targets::new()
        .with_default(Level::WARN)
        .with_target("winit", LevelFilter::OFF)
        .with_target("quasselinspector", LevelFilter::DEBUG)
        .with_target("libquassel", LevelFilter::TRACE);

    let env_filter = EnvFilter::from_default_env();

    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer().with_span_events(FmtSpan::ACTIVE))
        .with(filter)
        .with(env_filter)
        .init();

    debug!("starting");

    /*
     * setup shared data structures
     */

    let (ui_channel_tx, ui_channel_rx) = bounded::<server::UiMessage>(32);

    let state = State {
        inspector: InspectorState::default(),
        quassel: server::QuasselState::default(),
        logs: server::Log::default(),
        ui_channel_tx,
    };

    /*
     * setup tokio runtime
     */

    let runtime = tokio::runtime::Runtime::new().unwrap();
    let tokio_logs = state.logs.clone();
    let tokio_quassel = state.quassel.clone();
    let tokio_handle = thread::spawn(move || {
        runtime.block_on(async {
            while let Ok(msg) = ui_channel_rx.recv() {
                match msg {
                    server::UiMessage::Connect(server) => {
                        server
                            .connect(tokio_logs.clone(), tokio_quassel.clone())
                            .await
                    }
                }
            }
        })
    });

    /*
     * setup gui
     */
    let options = eframe::NativeOptions::default();

    let inspector = "Inspector".to_string();
    let logs = "Logs".to_string();

    let mut dock_state = egui_dock::DockState::new(vec![inspector, logs]);

    eframe::run_native(
        "Quassel Inspector",
        options,
        Box::new(|cc: &eframe::CreationContext<'_>| {
            match CTX.set(cc.egui_ctx.clone()) {
                Ok(_) => (),
                Err(_) => panic!("could not set context"),
            };

            Ok(Box::new(StateTracker {
                server: Server::default(),
                dock_state,
                state,
            }))
        }),
    )
    .unwrap();

    tokio_handle.join().unwrap();
}

trait Drawable {
    fn draw(&self, ui: &mut egui::Ui, tx: Sender<server::UiMessage>);
}
