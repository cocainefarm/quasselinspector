use crossbeam_channel::Sender;
use tracing::debug;

use super::UiMessage;

impl super::Server {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn draw(&mut self, ui: &mut egui::Ui, tx: Sender<UiMessage>) -> egui::Response {
        ui.centered_and_justified(|ui| {
            ui.set_max_size(egui::Vec2 { x: 280.0, y: 240.0 });
            ui.vertical(|ui| {
                ui.label("Server Name");
                ui.text_edit_singleline(&mut self.server_name);

                ui.add_space(5.0);
                ui.style_mut().spacing.text_edit_width = 225.0;

                ui.label("Listen Host & Port");
                ui.horizontal(|ui| {
                    ui.text_edit_singleline(&mut self.listen_host);
                    ui.add(egui::DragValue::new(&mut self.listen_port).speed(1));
                });

                ui.add_space(5.0);

                ui.label("Connection Settings");
                ui.horizontal(|ui| {
                    ui.text_edit_singleline(&mut self.settings.host);
                    ui.add(egui::DragValue::new(&mut self.settings.port).speed(1));
                });

                ui.add_space(2.0);
                ui.horizontal(|ui| {
                    ui.checkbox(&mut self.settings.tls, "TLS");
                    ui.checkbox(&mut self.settings.compression, "Compression");
                });

                ui.add_space(10.0);

                ui.horizontal(|ui| {
                    if ui.button("Connect").clicked() {
                        tx.send(UiMessage::Connect(self.clone())).unwrap();
                        self.connected = true;
                    };
                    ui.with_layout(egui::Layout::right_to_left(egui::Align::RIGHT), |ui| {
                        if ui.button("Reset").clicked() {
                            *self = Self::default();
                        };
                    })
                });
            })
        })
        .response
    }
}
