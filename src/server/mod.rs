use std::sync::Arc;

use anyhow::{bail, Error};

use egui::mutex::RwLock;
use libquassel::{
    frame::QuasselCodec,
    message::{self, Class, ConnAck, HandshakeMessage, Init, StatefulSyncableClient, Syncable},
    serialize::Deserialize,
    session::SessionManager,
};

use futures::{
    stream::{SplitSink, SplitStream},
    SinkExt, StreamExt,
};
use tokio::{
    io::{AsyncRead, AsyncReadExt, AsyncWrite, AsyncWriteExt},
    net::TcpStream,
};
use tokio_util::codec::Framed;

use tracing::{debug, trace};

mod connect;
mod ui;

#[derive(Clone, Debug)]
pub struct ServerSettings {
    pub tls: bool,
    pub compression: bool,
    pub host: String,
    pub port: u16,
}

impl Default for ServerSettings {
    fn default() -> Self {
        ServerSettings {
            tls: false,
            compression: false,
            host: String::from("localhost"),
            port: 4242,
        }
    }
}

#[derive(Clone, Debug)]
pub struct Server {
    pub server_name: String,
    pub listen_port: u16,
    pub listen_host: String,
    pub settings: ServerSettings,
    pub connected: bool,
}

impl Default for Server {
    fn default() -> Self {
        Self {
            server_name: "My Server".to_string(),
            listen_port: 4243,
            listen_host: "127.0.0.1".to_string(),
            settings: Default::default(),
            connected: false,
        }
    }
}

#[derive(Debug)]
pub enum UiMessage {
    Connect(Server),
}

#[derive(Debug)]
pub enum ClientState {
    Handshake,
    Connected,
}

#[derive(Clone, Debug)]
pub enum Message {
    Handshake(HandshakeMessage),
    SignalProxy(message::Message),
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Direction {
    ServerToClient,
    ClientToServer,
}

pub type Log = Arc<RwLock<Vec<(Direction, Message)>>>;
pub type QuasselState = Arc<RwLock<libquassel::session::Session>>;

impl Server {
    pub async fn init(
        &self,
        stream: &mut (impl AsyncRead + AsyncWrite + Unpin),
    ) -> Result<ConnAck, Error> {
        let init = Init::new()
            .tls(self.settings.tls)
            .compression(self.settings.compression);

        stream.write(&init.serialize()).await?;

        let mut buf = [0; 4];
        stream.read(&mut buf).await?;

        let (_, connack) = ConnAck::parse(&buf).unwrap();
        Ok(connack)
    }

    pub async fn handle_events(
        mut messages: tokio::sync::mpsc::Receiver<(Direction, Message)>,
        logs: Log,
        quassel: QuasselState,
    ) {
        while let Some(msg) = messages.recv().await {
            let mut logs = logs.write();
            let log = msg.1.clone();
            match msg {
                (direction, Message::Handshake(msg)) => {
                    logs.push((direction, log));

                    match msg {
                        HandshakeMessage::SessionInit(data) => {
                            let mut quassel = quassel.write();

                            quassel.session_init(data);
                        }
                        _ => (),
                    }
                }
                (direction, Message::SignalProxy(msg)) => {
                    logs.push((direction, log));

                    match msg {
                        message::Message::SyncMessage(msg) => {
                            let mut quassel = quassel.write();

                            quassel.sync(msg);
                        }
                        message::Message::RpcCall(_) => (),
                        message::Message::InitRequest(_) => (),
                        message::Message::InitData(data) => {
                            let mut quassel = quassel.write();
                            quassel.init(data);
                        }
                        message::Message::HeartBeat(_) => (),
                        message::Message::HeartBeatReply(_) => (),
                    }
                }
            }

            crate::CTX.get().unwrap().request_repaint();
        }
    }

    pub async fn run(
        mut stream: SplitStream<Framed<TcpStream, QuasselCodec>>,
        mut sink: SplitSink<Framed<TcpStream, QuasselCodec>, Vec<u8>>,
        mut state: ClientState,
        direction: Direction,
        messages: tokio::sync::mpsc::Sender<(Direction, Message)>,
    ) {
        // Start event loop
        while let Some(msg) = stream.next().await {
            let msg = msg.unwrap();
            sink.send(msg.to_vec()).await.unwrap();
            let msg = match state {
                ClientState::Handshake => Server::handle_login_message(&msg, &mut state, direction)
                    .await
                    .unwrap(),
                ClientState::Connected => Server::handle_message(&msg, direction).await.unwrap(),
            };

            messages.send((direction, msg)).await.unwrap();
        }
    }

    async fn handle_login_message(
        buf: &[u8],
        state: &mut ClientState,
        _direction: Direction,
    ) -> Result<Message, Error> {
        use libquassel::HandshakeDeserialize;

        trace!(target: "handshakemessage", "Received bytes: {:x?}", buf);
        match HandshakeMessage::parse(buf) {
            Ok((_size, res)) => {
                // info!("{}: {:#?}", direction, res);

                match res {
                    HandshakeMessage::SessionInit(_) => *state = ClientState::Connected,
                    HandshakeMessage::ClientLogin(_) => *state = ClientState::Connected,
                    _ => {}
                }

                return Ok(Message::Handshake(res));
            }
            Err(e) => bail!("failed to parse handshake message {}", e),
        }
    }

    async fn handle_message(buf: &[u8], direction: Direction) -> Result<Message, Error> {
        use libquassel::serialize::*;
        trace!(target: "message", "Received bytes: {:x?}", buf);

        match message::Message::parse(buf) {
            Ok((_size, res)) => {
                return Ok(Message::SignalProxy(res));
            }
            Err(e) => {
                bail!("failed to parse message {}", e);
            }
        }
    }
}
