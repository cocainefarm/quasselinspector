use libquassel::frame::QuasselCodec;

use futures::StreamExt;
use tokio::{
    io::{AsyncReadExt, AsyncWriteExt},
    net::{TcpListener, TcpStream},
    sync::mpsc,
};
use tokio_util::codec::{Decoder, Encoder, Framed};
use tracing::debug;

use super::{ClientState, Direction, Message, Server};

impl super::Server {
    pub async fn connect(&self, logs: super::Log, quassel: super::QuasselState) {
        debug!("starting connect");

        let mut s_server =
            TcpStream::connect(format!("{}:{}", self.settings.host, self.settings.port))
                .await
                .unwrap();

        let _connack = self.init(&mut s_server).await.unwrap();

        let codec = QuasselCodec::builder().compression(false).new_codec();
        let framed = Framed::new(s_server, codec);
        let (s_sink, s_stream) = framed.split();

        let listener = TcpListener::bind((self.listen_host.as_ref(), self.listen_port))
            .await
            .unwrap();
        let (mut client, _) = listener.accept().await.unwrap();

        //
        // Setup Listener
        //

        {
            let (mut c_stream, mut c_sink) = client.split();

            let mut init = [0; 12];
            let n = c_stream.peek(&mut init).await.unwrap();
            c_stream.read(&mut init[..n]).await.unwrap();
            let init = libquassel::message::Init::parse(&init);
            debug!("send init bytes: {:?}", init);

            c_sink.write(&[0x0, 0x0, 0x0, 0x2]).await.unwrap();
        }

        let codec = QuasselCodec::builder().compression(false).new_codec();
        let framed = Framed::new(client, codec);
        let (c_sink, c_stream) = framed.split();

        // Start Processing

        let s_state = ClientState::Handshake;
        let c_state = ClientState::Handshake;

        let (ui_channel_tx, ui_channel_rx) = mpsc::channel(100);

        tokio::join!(
            Server::run(
                s_stream,
                c_sink,
                s_state,
                Direction::ServerToClient,
                ui_channel_tx.clone()
            ),
            Server::run(
                c_stream,
                s_sink,
                c_state,
                Direction::ClientToServer,
                ui_channel_tx
            ),
            Server::handle_events(ui_channel_rx, logs, quassel)
        );
    }
}
