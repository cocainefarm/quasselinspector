use egui_derive::row;
use egui_extras::{Column, TableBuilder};
use libquassel::message::objects::BufferViewManager;

use crate::Drawable;

use super::{TABLE_HEADER, TABLE_ROW};

impl Drawable for BufferViewManager {
    fn draw(&self, ui: &mut egui::Ui, tx: crossbeam_channel::Sender<crate::server::UiMessage>) {
        for (id, config) in &self.buffer_view_configs {
            ui.collapsing(format!("Buffer View Config: {}", id.to_string()), |ui| {
                if let Some(config) = config {
                    ui.push_id(id, |ui| {
                        TableBuilder::new(ui)
                            .column(Column::auto_with_initial_suggestion(400.0).resizable(true))
                            .column(Column::remainder())
                            .header(TABLE_HEADER, |mut header| {
                                header.col(|ui| {
                                    ui.heading("Field");
                                });
                                header.col(|ui| {
                                    ui.heading("Value");
                                });
                            })
                            .body(|mut body| {
                                row!(
                                    ui.label("buffers"),
                                    ui.label(format!("{:?}", config.buffers))
                                );
                                row!(
                                    ui.label("removed_buffers"),
                                    ui.label(format!("{:?}", config.removed_buffers))
                                );
                                row!(
                                    ui.label("temporarily_removed_buffers"),
                                    ui.label(format!("{:?}", config.temporarily_removed_buffers))
                                );
                                row!(
                                    ui.label("buffer_view_id"),
                                    ui.label(config.buffer_view_id.to_string())
                                );
                                row!(
                                    ui.label("buffer_view_name"),
                                    ui.label(&config.buffer_view_name)
                                );
                                row!(
                                    ui.label("network_id"),
                                    ui.label(config.network_id.to_string())
                                );
                                row!(
                                    ui.label("add_new_buffers_automatically"),
                                    ui.label(config.add_new_buffers_automatically.to_string())
                                );
                                row!(
                                    ui.label("sort_alphabetically"),
                                    ui.label(config.sort_alphabetically.to_string())
                                );
                                row!(
                                    ui.label("hide_inactive_buffers"),
                                    ui.label(config.hide_inactive_buffers.to_string())
                                );
                                row!(
                                    ui.label("hide_inactive_networks"),
                                    ui.label(config.hide_inactive_networks.to_string())
                                );
                                row!(
                                    ui.label("disable_decoration"),
                                    ui.label(config.disable_decoration.to_string())
                                );
                                row!(
                                    ui.label("allowed_buffer_types"),
                                    ui.label(config.allowed_buffer_types.to_string())
                                );
                                row!(
                                    ui.label("minimum_activity"),
                                    ui.label(config.minimum_activity.to_string())
                                );
                                row!(
                                    ui.label("show_search"),
                                    ui.label(config.show_search.to_string())
                                );
                            });
                    });
                } else {
                    ui.label("None");
                }
            });
        }
    }
}
