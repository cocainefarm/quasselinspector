use egui_extras::{Column, TableBuilder};
use libquassel::message::objects::BufferSyncer;

use crate::Drawable;

use super::{TABLE_HEADER, TABLE_ROW};

impl Drawable for BufferSyncer {
    fn draw(&self, ui: &mut egui::Ui, tx: crossbeam_channel::Sender<crate::server::UiMessage>) {
        ui.push_id(0, |ui| {
            ui.label("Activities");
            TableBuilder::new(ui)
                .column(Column::initial(100.0))
                .column(Column::remainder())
                .header(TABLE_HEADER, |mut header| {
                    header.col(|ui| {
                        ui.heading("ID");
                    });
                    header.col(|ui| {
                        ui.heading("Message Type");
                    });
                })
                .body(|mut body| {
                    for (id, msg_type) in &self.activities {
                        body.row(TABLE_ROW, |mut row| {
                            row.col(|ui| {
                                ui.label(id.to_string());
                            });
                            row.col(|ui| {
                                ui.label(format!("{:#?}", msg_type));
                            });
                        });
                    }
                });
        });

        ui.push_id(1, |ui| {
            ui.label("Highlight Counts");
            TableBuilder::new(ui)
                .column(Column::initial(100.0))
                .column(Column::remainder())
                .header(TABLE_HEADER, |mut header| {
                    header.col(|ui| {
                        ui.heading("ID");
                    });
                    header.col(|ui| {
                        ui.heading("Count");
                    });
                })
                .body(|mut body| {
                    for (id, count) in &self.highlight_counts {
                        body.row(TABLE_ROW, |mut row| {
                            row.col(|ui| {
                                ui.label(id.to_string());
                            });
                            row.col(|ui| {
                                ui.label(count.to_string());
                            });
                        });
                    }
                });
        });

        ui.push_id(2, |ui| {
            ui.label("Last Seen Message");
            TableBuilder::new(ui)
                .column(Column::initial(100.0))
                .column(Column::remainder())
                .header(TABLE_HEADER, |mut header| {
                    header.col(|ui| {
                        ui.heading("ID");
                    });
                    header.col(|ui| {
                        ui.heading("Message ID");
                    });
                })
                .body(|mut body| {
                    for (id, msg) in &self.last_seen_msg {
                        body.row(TABLE_ROW, |mut row| {
                            row.col(|ui| {
                                ui.label(id.to_string());
                            });
                            row.col(|ui| {
                                ui.label(msg.to_string());
                            });
                        });
                    }
                });
        });

        ui.push_id(3, |ui| {
            ui.label("Marker Line");
            TableBuilder::new(ui)
                .column(Column::initial(100.0))
                .column(Column::remainder())
                .header(TABLE_HEADER, |mut header| {
                    header.col(|ui| {
                        ui.heading("ID");
                    });
                    header.col(|ui| {
                        ui.heading("Message ID");
                    });
                })
                .body(|mut body| {
                    for (id, msg) in &self.marker_line {
                        body.row(TABLE_ROW, |mut row| {
                            row.col(|ui| {
                                ui.label(id.to_string());
                            });
                            row.col(|ui| {
                                ui.label(msg.to_string());
                            });
                        });
                    }
                });
        });
    }
}
