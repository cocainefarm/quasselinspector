use egui_derive::row;
use egui_extras::{Column, TableBuilder};
use libquassel::message::objects::CoreInfo;

use crate::Drawable;

use super::{TABLE_HEADER, TABLE_ROW};

impl Drawable for CoreInfo {
    fn draw(&self, ui: &mut egui::Ui, tx: crossbeam_channel::Sender<crate::server::UiMessage>) {
        ui.label(format!("Version: {}", self.core_data.quassel_version));
        ui.label(format!("Build Date: {}", self.core_data.quassel_build_date));
        ui.label(format!("Start Time: {}", self.core_data.start_time));

        ui.add_space(20.0);
        ui.label(format!(
            "Connected Clients: {:?}",
            self.core_data.session_connected_clients
        ));

        for (id, client) in self
            .core_data
            .session_connected_client_data
            .iter()
            .enumerate()
        {
            ui.push_id(id, |ui| {
                TableBuilder::new(ui)
                    .column(Column::initial(100.0))
                    .column(Column::remainder())
                    .header(TABLE_HEADER, |mut header| {
                        header.col(|ui| {
                            ui.heading("Field");
                        });
                        header.col(|ui| {
                            ui.heading("Value");
                        });
                    })
                    .body(|mut body| {
                        row!(ui.label("ID"), ui.label(client.id.to_string()));
                        row!(ui.label("Remote Address"), ui.label(&client.remote_address));
                        row!(ui.label("Client Version"), ui.label(&client.client_version));
                        row!(
                            ui.label("Client Date"),
                            ui.label(&client.client_version_date)
                        );
                        row!(
                            ui.label("Connected Since"),
                            ui.label(format!("{:?}", client.connected_since))
                        );
                        row!(ui.label("Secure"), ui.label(client.secure.to_string()));
                        row!(ui.label("Features"), ui.label(client.features.to_string()));
                        row!(
                            ui.label("Feature List"),
                            ui.label(format!("{:?}", client.feature_list))
                        );
                    });
            });
        }
    }
}
