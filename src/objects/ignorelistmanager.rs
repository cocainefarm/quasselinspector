use egui_derive::{header, row};
use egui_extras::{Column, TableBuilder};
use libquassel::message::objects::IgnoreListManager;

use crate::Drawable;

use super::{TABLE_HEADER, TABLE_ROW};

impl Drawable for IgnoreListManager {
    fn draw(&self, ui: &mut egui::Ui, tx: crossbeam_channel::Sender<crate::server::UiMessage>) {
        TableBuilder::new(ui)
            .columns(Column::remainder(), 7)
            .header(TABLE_HEADER, |mut header| {
                header!(ui.label("type"));
                header!(ui.label("rule"));
                header!(ui.label("regex"));
                header!(ui.label("strictness"));
                header!(ui.label("scope"));
                header!(ui.label("scope rule"));
                header!(ui.label("active"));
            })
            .body(|mut body| {
                for item in &self.ignore_list {
                    row!(
                        ui.label(format!("{:?}", item.ignore_type)),
                        ui.label(&item.ignore_rule),
                        ui.label(item.is_regex.to_string()),
                        ui.label(format!("{:?}", item.strictness)),
                        ui.label(format!("{:?}", item.scope)),
                        ui.label(&item.scope_rule),
                        ui.label(item.is_active.to_string()),
                    );
                }
            });
    }
}
