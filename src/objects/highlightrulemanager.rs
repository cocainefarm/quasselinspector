use egui_derive::{header, row};
use egui_extras::{Column, TableBuilder};
use libquassel::message::objects::HighlightRuleManager;

use crate::Drawable;

use super::{TABLE_HEADER, TABLE_ROW};

impl Drawable for HighlightRuleManager {
    fn draw(&self, ui: &mut egui::Ui, tx: crossbeam_channel::Sender<crate::server::UiMessage>) {
        ui.horizontal(|ui| {
            ui.label(format!("highlight nick type: {:?}", &self.highlight_nick));
            ui.checkbox(
                &mut self.nicks_case_sensitive.clone(),
                "nicks case sensitive",
            );
        });

        TableBuilder::new(ui)
            .columns(Column::remainder(), 8)
            .header(TABLE_HEADER, |mut header| {
                header!(ui.label("ID"));
                header!(ui.label("Name"));
                header!(ui.label("Regex"));
                header!(ui.label("Case Sensitive"));
                header!(ui.label("Enabled"));
                header!(ui.label("Inverse"));
                header!(ui.label("Sender"));
                header!(ui.label("Channel"));
            })
            .body(|mut body| {
                for rule in &self.highlight_rule_list {
                    row!(
                        ui.label(rule.id.to_string()),
                        ui.label(&rule.name),
                        ui.label(rule.is_regex.to_string()),
                        ui.label(rule.is_case_sensitive.to_string()),
                        ui.label(rule.is_enabled.to_string()),
                        ui.label(rule.is_inverse.to_string()),
                        ui.label(&rule.sender),
                        ui.label(&rule.channel),
                    );
                }
            });
    }
}
