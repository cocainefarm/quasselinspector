use libquassel::message::objects::Identity;

use egui_derive::{header, row};
use egui_extras::{Size, TableBuilder};

use super::{TABLE_HEADER, TABLE_ROW};
use crate::Drawable;

impl Drawable for Vec<Identity> {
    fn draw(&self, ui: &mut egui::Ui, tx: crossbeam_channel::Sender<crate::server::UiMessage>) {
        for ident in self {
            ui.collapsing(&ident.identity_name, |ui| {
                egui::Grid::new("some_unique_id").show(ui, |ui| {
                    ui.label("id");
                    ui.label(ident.identity_id.to_string());
                    ui.end_row();

                    ui.label("name");
                    ui.label(&ident.identity_name);
                    ui.end_row();

                    ui.label("real name");
                    ui.label(&ident.real_name);
                    ui.end_row();

                    ui.label("nicks");
                    ui.label(format!("{:?}", ident.nicks));
                    ui.end_row();

                    ui.label("away nick");
                    ui.label(&ident.away_nick);
                    ui.end_row();

                    ui.label("away nick enabled");
                    ui.label(ident.away_nick_enabled.to_string());
                    ui.end_row();

                    ui.label("away reason");
                    ui.label(&ident.away_reason);
                    ui.end_row();

                    ui.label("away reason enabled");
                    ui.label(ident.away_reason_enabled.to_string());
                    ui.end_row();

                    ui.label("auto away time");
                    ui.label(ident.auto_away_time.to_string());
                    ui.end_row();

                    ui.label("auto away enabled");
                    ui.label(ident.auto_away_enabled.to_string());
                    ui.end_row();

                    ui.label("auto away reason");
                    ui.label(&ident.auto_away_reason);
                    ui.end_row();

                    ui.label("auto away reason enabled");
                    ui.label(ident.auto_away_reason_enabled.to_string());
                    ui.end_row();

                    ui.label("ident");
                    ui.label(&ident.ident);
                    ui.end_row();

                    ui.label("kick reason");
                    ui.label(&ident.kick_reason);
                    ui.end_row();

                    ui.label("part reason");
                    ui.label(&ident.part_reason);
                    ui.end_row();

                    ui.label("quit reason");
                    ui.label(&ident.quit_reason);
                    ui.end_row();
                });
            });
        }
    }
}
