use egui_derive::row;
use egui_extras::{Column, TableBuilder};
use libquassel::message::objects::IrcChannel;

use crate::Drawable;

use super::{TABLE_HEADER, TABLE_ROW};

impl Drawable for IrcChannel {
    fn draw(&self, ui: &mut egui::Ui, tx: crossbeam_channel::Sender<crate::server::UiMessage>) {
        ui.label(&self.name);
        ui.label(format!("topic: {}", &self.topic));

        TableBuilder::new(ui)
            .column(Column::initial(100.0))
            .column(Column::remainder())
            .header(TABLE_HEADER, |mut header| {
                header.col(|ui| {
                    ui.heading("Field");
                });
                header.col(|ui| {
                    ui.heading("Value");
                });
            })
            .body(|mut body| {
                row!(
                    ui.label("channel_modes_a"),
                    ui.label(format!("{:?}", self.chan_modes.channel_modes_a))
                );
                row!(
                    ui.label("channel_modes_b"),
                    ui.label(format!("{:?}", self.chan_modes.channel_modes_b))
                );
                row!(
                    ui.label("channel_modes_c"),
                    ui.label(format!("{:?}", self.chan_modes.channel_modes_c))
                );
                row!(ui.label("channel_modes_d"), ui.label(&self.chan_modes.channel_modes_d));
                row!(ui.separator(), ui.separator());
                row!(
                    ui.label("user_modes"),
                    ui.label(format!("{:?}", &self.user_modes))
                );
            });
    }
}
