use libquassel::message::objects::CertManager;

use crate::Drawable;

impl Drawable for CertManager {
    fn draw(&self, ui: &mut egui::Ui, tx: crossbeam_channel::Sender<crate::server::UiMessage>) {
        ui.horizontal(|ui| {
            ui.label("SSL Key");
            ui.label(&self.ssl_key);
        });
        ui.horizontal(|ui| {
            ui.label("SSL Cert");
            ui.label(&self.ssl_cert);
        });
    }
}
