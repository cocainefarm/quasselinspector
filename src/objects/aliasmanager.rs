use egui_extras::{Column, TableBuilder};
use libquassel::message::objects::AliasManager;

impl crate::Drawable for AliasManager {
    fn draw(&self, ui: &mut egui::Ui, _tx: crossbeam_channel::Sender<crate::server::UiMessage>) {
        TableBuilder::new(ui)
            .column(Column::initial(100.0))
            .column(Column::remainder())
            .header(20.0, |mut header| {
                header.col(|ui| {
                    ui.heading("Name");
                });
                header.col(|ui| {
                    ui.heading("Expansion");
                });
            })
            .body(|mut body| {
                for alias in &self.aliases {
                    body.row(15.0, |mut row| {
                        row.col(|ui| {
                            ui.label(&alias.name);
                        });
                        row.col(|ui| {
                            ui.label(&alias.expansion);
                        });
                    });
                }
            });
    }
}
