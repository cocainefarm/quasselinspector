static TABLE_HEADER: f32 = 20.0;
static TABLE_ROW: f32 = 15.0;

mod aliasmanager;
mod buffersyncer;
// mod backlogmanager;
mod bufferviewmanager;
// mod bufferviewconfig;
mod certmanager;
mod coreinfo;
mod highlightrulemanager;
mod identities;
mod ignorelistmanager;
mod ircchannel;
// mod ircuser;
mod network;
// mod networkinfo;
