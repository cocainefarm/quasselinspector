use libquassel::message::objects::Network;

use crate::Drawable;

impl Drawable for Network {
    fn draw(&self, ui: &mut egui::Ui, tx: crossbeam_channel::Sender<crate::server::UiMessage>) {
        ui.collapsing(&self.network_info.network_name, |ui| {
            ui.horizontal(|ui| {
                ui.label(format!("nick: {}", &self.my_nick));
                ui.label(format!("latency: {}ms", self.latency.to_string()));
                ui.label(format!("server: {}", &self.current_server));
                ui.label(format!("state: {:?}", self.connection_state));
            });

            ui.add_space(20.0);
            ui.label("Channels");
            for (_, channel) in &self.irc_channels {
                channel.draw(ui, tx.clone())
            }
        });
    }
}
