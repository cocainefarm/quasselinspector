extern crate quote;
extern crate syn;

use quote::quote;
use syn::{
    bracketed,
    parse::{Parse, ParseStream},
    parse_macro_input,
    punctuated::Punctuated,
    token, Expr, Result, Token,
};

#[derive(Debug)]
struct Row {
    fields: Punctuated<Expr, Token![,]>,
}

impl Parse for Row {
    fn parse(input: ParseStream) -> Result<Self> {
        Ok(Row {
            fields: input.parse_terminated(Expr::parse)?,
        })
    }
}

#[proc_macro]
pub fn row(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as Row);
    let values = input.fields.iter();

    let gen = quote! {
        body.row(TABLE_ROW, |mut row| {
            #(
                row.col(|ui| {
                    #values;
                });
            )*
        });
    };

    gen.into()
}

#[derive(Debug)]
struct Header {
    fields: Punctuated<Expr, Token![,]>,
}

impl Parse for Header {
    fn parse(input: ParseStream) -> Result<Self> {
        Ok(Header {
            fields: input.parse_terminated(Expr::parse)?,
        })
    }
}

#[proc_macro]
pub fn header(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as Header);
    let values = input.fields.iter();

    let gen = quote! {
        #(
            header.col(|ui| {
                #values;
            });
        )*
    };

    gen.into()
}
